@load base/protocols/conn

type trojan_detector_state: record { 
    step: count;
    start: time;
    expiration: time;
};

##
##type trojan_detector_point: record {
##    nw_addr: addr;
##    tp_port: port;
##};

type trojan_detector_state_table: table[addr] of trojan_detector_state;
global td_state_table  = trojan_detector_state_table();


event trojan_detector_polling()
{
#    print fmt("poll!");
    if ( |td_state_table| > 0 ) {
        local ctime = current_time();
        for ( p in td_state_table ) {
            if ( td_state_table[p]$expiration < ctime ) {
                print fmt("timeout!");
                delete td_state_table[p];
            }
        }
    }
    schedule 3 secs { trojan_detector_polling()};
}

event bro_init()
{
	print fmt("Bro init!");
    event trojan_detector_polling();
}

event protocol_confirmation(c: connection, atype: Analyzer::Tag, aid:count)
{
	if ( atype == Analyzer::ANALYZER_SSH ) {
        local aip = c$id$resp_h;
         if(aip !in td_state_table){
            td_state_table[aip] = trojan_detector_state($step = 1, $start = current_time(), $expiration = current_time() + 60sec);
            print fmt("ssh!");
        }
    }
    if ( atype == Analyzer::ANALYZER_IRC ) {
        local bip = c$id$orig_h;
        if ( bip in td_state_table ) {
            if ( td_state_table[bip]$step == 4 ) {
                print fmt("irc!");
                print fmt("ATTACK!");
                delete td_state_table[bip];
            }
        }
    }
}

event ftp_request(c: connection, command: string, arg: string)
{
    local aip = c$id$orig_h;
    if ( aip in td_state_table ) {
        if ( td_state_table[aip]$step == 2) {
            td_state_table[aip]$step += 1;
            print fmt("ftp request!");
            print fmt("ATTACK!");
            delete td_state_table[aip];

        }
    }
}

event http_request(c: connection, method: string, original_URI: string,
                    unescaped_RUI: string, version: string) 
{
    local aip = c$id$orig_h;
    if ( aip in td_state_table ) {
        if ( td_state_table[aip]$step == 1 ) {
            td_state_table[aip]$step += 1;
            print fmt("HTTP request!");
        }
    }
}

