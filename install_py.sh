#!/bin/bash
#-----------------------------------------------------------------------------
# If you want to use any of the python scripts provided by this repository,
# run this script first. 
# This script tunes the system such that the provided python scripts can
# work. 
#
#-----------------------------------------------------------------------------


python3 ./get-pip.py 
pip3 install pexpect --upgrade 

python2 ./get-pip.py 
pip2 install pexpect --upgrade 


