
To install:
    visit https://www.bro.org/sphinx/install/index.html 
    Generally, go into each directories and install each of the 
    prerequirments. Then get into bro directory.

To make a minimal starting configuration:
    visit https://www.bro.org/sphinx/quickstart/ 

Quickstart configuration check list:
    - $PREFIX/etc/node.cfg -- change interface, e.g., eth2
    - $PREFIX/etc/networks.cfg -- change networks
    - $PREFIX/etc/broctl.cfg -- change MailTo
    - $PREFIX/share/bro/site/local.bro -- add "redef ignore_checksums = T;" at the end. 
      (https://www.bro.org/documentation/faq.html#why-isn-t-bro-producing-the-logs-i-expect-a-note-about-checksums)
    - broctl 
    - > install 
    - > start 
    
Useful TCP tools:
    - tcpprep
    - tcprewrite
    - tcpreplay

Command examples:
    - tcpprep --cidr=224.218.0.0/32,130.127.0.0/16 -i data.pcap -o data.cache 
      This will preprocess data.pcap. The give networks are servers' network. 
    - tcprewirte --enet-dmac=BRO_MAC,BRO_MAC --enet-smac=SOURCE_MAC,SOURCE_MAC \
                 --srcipmap=SOURCE_IP/32:APPROPRIATE_IP/32,SOURCE_IP/32:APPROPRIATE_IP/32 \
                 --dstipmap=SOURCE_IP/32:APPROPRIATE_IP/32,SOURCE_IP/32:APPROPRIATE_IP/32 \
                 -i data.pcap -c data.cache -o data.rewrite.pcap 
    - tcpreplay -i eth2 --mbps=10 data.rewrite.pcap



